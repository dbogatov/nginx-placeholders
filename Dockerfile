FROM dbogatov/docker-images:nginx-latest

MAINTAINER Dmytro Bogatov dmytro@dbogatov.org

WORKDIR /srv
COPY index.html .

RUN rm -rf /etc/nginx/conf.d/default.conf
COPY nginx.conf /etc/nginx/conf.d/

CMD ["nginx", "-g", "daemon off;"]
