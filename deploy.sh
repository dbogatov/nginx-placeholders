#!/usr/bin/env bash 

set -e

for site in ./pages/*.html; 
do 
	SITEONLY=$(basename $site)
	IMAGE="${SITEONLY%.*}"

	echo "Deploying $SITEONLY..."

	rm -f ./index.html
	cp $site ./index.html
	cp ./configs/$IMAGE.conf ./nginx.conf
	
	docker build -t registry.dbogatov.org/dbogatov/nginx-placeholders/$IMAGE .

	docker push registry.dbogatov.org/dbogatov/nginx-placeholders/$IMAGE

done

rm -f ./index.conf

echo "Deploy succeeded"
